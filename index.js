const express = require ("express");
const mongoose = require ("mongoose");

const app = express();
const port = 3001;

// MongoDB Connection
mongoose.connect("mongodb+srv://jeniezuitt:admin@cluster0.vsejk.mongodb.net/b177-to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Set notification for connection success or failure
// Declare Connection to the database
let db = mongoose.connection;

// If a connection error occured, output a message in the console
db.on("error", console.error.bind(console, "connection error"));

// If a connection successful, output a message in the console
db.once("open", ()=> console.log("Were connected to the cloud database"));

// Create a Task Schema

// Create moongose object
// Schema - Represent Structure of the Documents and contains data types
const taskSchema = new mongoose.Schema({
	// Properties
	name: String,
	status: {
		type: String,
		default: "pending" //Default values are the predefined values for a field
	}
})

// Create Models
// Models- provides programming interface query and manipulate
// Server > Schema > Models(hold database) > Collection (MongoDB)
const Task = mongoose.model("Task", taskSchema);



// [Activity] User Schema
const userSchema = new mongoose.Schema({
    username : String,
    password : String
})

// [Activity] User Model
const User = mongoose.model("User", userSchema);

// to read JSON data
app.use(express.json())
// to read forms data
app.use(express.urlencoded({extended:true}));


// [Activity] Register a user
app.post("/signup", (req, res)=> {

	// Finds for a document with the matching username provided in the client/Postman
	User.findOne({ username : req.body.username }, (err, result) => {

		// Check for duplicates
		if(result != null && result.username == req.body.username){

			return res.send("Duplicate username found");

		// No duplicates found
		} else {

			// If the username and password are both not blank
			if(req.body.username !== '' && req.body.password !== ''){

				// Create/instantiate a "newUser" from the "User" model
                let newUser = new User({
                    username : req.body.username,
                    password : req.body.password
                });
    
    			// Create a document in the database
                newUser.save((saveErr, savedTask) => {

                    // If an error occurred
                    if(saveErr){

                    	// Return an error in the client/Postman
                        return console.error(saveErr);

                    // If no errors are found
                    } else {

                    	// Send a response back to the client/Postman of "created"
                        return res.status(201).send("New user registered");

                    }

                })

            // If the "username" or "password" was left blank
            } else {

            	/// Send a response back to the client/Postman of "created"
                return res.send("BOTH username and password must be provided.");
            }			
		}
	})
})

// [Stretch goal] Retrieve all registered user
app.get("/users", (req, res) => {
	User.find({}, (err, result) => {
		if(err){
			return console.error(err);
		}
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
})




// Create a POST route to create a new task
app.post("/tasks", (req,res) => {
	Task.findOne({name : req.body.name}, (err, result) => {
		// if a document was found and the documents name matches the information sent via the client/postman
		if (result != null && result.name == req.body.name) {
			return res.send("Duplicate task found")
		}
		// if no document found
		else {
			// Create a new task and save it to database
			let newTask = new Task ({
			name: req.body.name
			})
			// Save to database
			newTask.save((saveErr, savedTask) => {
				// If there are error in savings
				if (saveErr){
					return console.error(saveErr)
				}
				// that if no error found while creating the documents
				else {
					return res.status(201).send("New task created");
				}
			})
		}
	})
})

// Create a GET request to retrieve all the tasks
app.get("/tasks", (req, res) => {
	Task.find({},(err, result) => {
		// if an error occured
		if(err){
			// will print any errors found in the console
			return console.log(err);
		} 
		// if no errors found
		else {
			return res.status(200).json({
				data : result
			})
		}
	})
})


app.listen(port, () => console.log(`Server running at port ${port}`));